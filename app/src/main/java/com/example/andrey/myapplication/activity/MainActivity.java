package com.example.andrey.myapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.andrey.myapplication.R;
import com.example.andrey.myapplication.fragment.ListPhotoFragment;
import com.example.andrey.myapplication.fragment.ThemeSelectionFragment;
import com.example.andrey.myapplication.helpers.PreferenceHelper;

/**
 * Главная активность с навигационным меню. Будет хостом для всех фрагментов
 */
public class MainActivity extends AppCompatActivity {

    protected NavigationView mNavigationView; // навигационное меню
    private Toolbar mToolbar;
    private DrawerLayout mDrawer;

    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme();
        initActivity();
    }

    public void initActivity() {
        setContentView(R.layout.activity_navigation_fragment);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        // утсановка toolbar
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(getNavigationItemListener());


        startFragment(ListPhotoFragment.newInstance());
    }

    private void setTheme() {
        int theme = PreferenceHelper.getInstance(this).getTheme();
        setTheme((theme == -1) ? R.style.AppTheme_NoActionBar_Blue : theme);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public NavigationView.OnNavigationItemSelectedListener getNavigationItemListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // Handle navigation view item clicks here.
                int id = item.getItemId();

                switch(id) {
                    case R.id.nav_main:
                        startFragment(ListPhotoFragment.newInstance());
                        break;
                    case R.id.nav_theme_selection:
                        startFragment(ThemeSelectionFragment.newInstance());
                        break;
                    default:
                        break;
                }

                mDrawer.closeDrawer(GravityCompat.START);
                return true;
            }
        };
    }

    private void startFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }


}
