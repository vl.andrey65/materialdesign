package com.example.andrey.myapplication.fragment;

import com.example.andrey.myapplication.model.ImageCard;

interface BottomNavigationFragmentCallback {
    void updateCurrentFragment();

    void addItemToList(ImageCard imageCard);
}
