package com.example.andrey.myapplication.model;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;

/**
 * Класс-хранилище всех изображений
 */
public class ImageCardStorage {

    private static ImageCardStorage sInstance;
    private ArrayList<ImageCard> mCardList;

    private ImageCardStorage() {
        mCardList = new ArrayList<>();
    };

    public static ImageCardStorage getInstance() {
        if (sInstance == null) {
            sInstance = new ImageCardStorage();
        }
        return sInstance;
    }

    public ArrayList<ImageCard> getCardList() {
        return new ArrayList<ImageCard>(mCardList);
    }

    public ArrayList<ImageCard> getCardListLike() {
        ArrayList<ImageCard> cardList = new ArrayList<>();
        for (ImageCard item : mCardList) {
            if (item.isLike()) {
                cardList.add(item);
            }
        }
        return cardList;
    }

    public void setCardList(ArrayList<ImageCard> cardList) {
        mCardList = cardList;
    }

    public void initData(Context context) {
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File[] storageFiles = storageDir.listFiles();
        for (int i = 0; i < storageFiles.length; i++) {
            ImageCard imageCard = new ImageCard();
            File file = new File(storageDir.getAbsolutePath() + "/" + storageFiles[i].getName());
            if (file.exists()) {
                imageCard.setFileAndBitmap(file);
                if (i % 2 == 0) {
                    imageCard.setLike(true);
                }
                mCardList.add(imageCard);
            }
        }
    }

    public void addItem(ImageCard imageCard) {
        mCardList.add(imageCard);
    }
}
