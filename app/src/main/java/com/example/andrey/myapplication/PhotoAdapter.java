package com.example.andrey.myapplication;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.andrey.myapplication.model.ImageCard;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ImageHolder> implements Serializable {

    private final int VIEW_TYPE_LIKE = 1;
    private final int VIEW_TYPE_NOT_LIKE = 0;

    private Context mContext;
    private ArrayList<ImageCard> mItems;

    private OnClickListener mOnClickListener;

    @Override
    public void onBindViewHolder(@NonNull ImageHolder holder, final int position) {
        holder.bind(mItems.get(position));
        holder.itemView.setOnClickListener(view -> mOnClickListener.onClickItem(mItems.get(position)));
        holder.itemView.setOnLongClickListener(view -> {
            mOnClickListener.onLongClickItem(mItems.get(position));
            return true;
        });
        holder.mImageLike.setOnClickListener((view) -> {
            mOnClickListener.onClickLike(mItems.get(position), position);
        });
    }

    public PhotoAdapter(Context context, ArrayList<ImageCard> items) {
        mContext = context;
        mItems = items;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = null;
        if (viewType == VIEW_TYPE_LIKE) {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.list_item_like_image, viewGroup, false);
        } else {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.list_item_image, viewGroup, false);
        }

        return new ImageHolder(itemView);
    }

    public interface OnClickListener {
        void onClickItem(ImageCard item);

        void onLongClickItem(ImageCard item);

        void onClickLike(ImageCard item, int pos);
    }

    @Override
    public int getItemViewType(int position) {
        if (mItems.get(position).isLike()) {
            return VIEW_TYPE_LIKE;
        } else {
            return VIEW_TYPE_NOT_LIKE;
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void updateData(ArrayList<ImageCard> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public void addItem(ImageCard item) {
        mItems.add(item);
        notifyItemInserted(mItems.size());
    }

    public void removeItem(ImageCard item) {
        for (int i = 0; i < mItems.size(); i++) {
            if (mItems.get(i).equals(item)) {
                mItems.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, mItems.size());
                break;
            }
        }
    }

    public class ImageHolder extends RecyclerView.ViewHolder implements Serializable {

        @BindView(R.id.image)
        ImageView mImage;
        @BindView(R.id.like)
        ImageView mImageLike;

        public ImageHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(ImageCard item) {
            mImage.setImageBitmap(item.getBitmap());
        }
    }
}
