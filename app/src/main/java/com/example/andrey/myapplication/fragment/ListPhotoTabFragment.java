package com.example.andrey.myapplication.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.andrey.myapplication.PhotoAdapter;
import com.example.andrey.myapplication.R;
import com.example.andrey.myapplication.model.ImageCard;
import com.example.andrey.myapplication.model.ImageCardStorage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListPhotoTabFragment extends Fragment
        implements ListPhotoTabFragmentCallback {

    private static final String DIALOG_BIG_PHOTO = "BigPhotoFragment";
    public static final String DISPLAY_TYPE_ALL = "DISPLAY_TYPE_ALL";
    public static final String DISPLAY_TYPE_LIKE = "DISPLAY_TYPE_LIKE";
    private static final String ARG_DISPLAY_TYPE = "ARG_DISPLAY_TYPE";

    private Unbinder mUnbinder;
    private PhotoAdapter mAdapter;
    private GridLayoutManager mLayoutManager;
    private final int mSpanCount = 3;
    private String mDisplayType;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    public static ListPhotoTabFragment newInstance(String displayType) {

        Bundle args = new Bundle();
        args.putString(ARG_DISPLAY_TYPE, displayType);

        ListPhotoTabFragment fragment = new ListPhotoTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDisplayType = getArguments().getString(ARG_DISPLAY_TYPE, DISPLAY_TYPE_ALL);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_photo_tab, container, false);
        mUnbinder = ButterKnife.bind(this, v);

        mLayoutManager = new GridLayoutManager(getActivity(), mSpanCount);
        mRecyclerView.setLayoutManager(mLayoutManager);

        initRecycler();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void initRecycler() {
        switch (mDisplayType) {
            case DISPLAY_TYPE_ALL:
                updateRecycler(ImageCardStorage.getInstance().getCardList());
                break;
            case DISPLAY_TYPE_LIKE:
                updateRecycler(ImageCardStorage.getInstance().getCardListLike());
                break;
            default:
                break;
        }
    }

    public void updateRecycler(ArrayList<ImageCard> data) {
        if (mAdapter == null) {
            mAdapter = new PhotoAdapter(getActivity(), data);
            mAdapter.setOnClickListener(new PhotoAdapter.OnClickListener() {
                @Override
                public void onClickItem(ImageCard imageCard) {
                    FragmentManager fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                    BigPhotoFragment fragment = BigPhotoFragment.newInstance(imageCard);
                    fragment.show(fm, DIALOG_BIG_PHOTO);
                }

                @Override
                public void onLongClickItem(ImageCard item) {
                    openDeleteDialog(item);
                }

                @Override
                public void onClickLike(ImageCard item, int pos) {
                    if (item.isLike()) {
                        item.setLike(false);
                        if (mDisplayType.equals(DISPLAY_TYPE_ALL)) {
                            mAdapter.notifyItemChanged(pos);
                        } else if (mDisplayType.equals(DISPLAY_TYPE_LIKE)) {
                            mAdapter.removeItem(item);
                        }
                    } else {
                        item.setLike(true);
                        mAdapter.notifyItemChanged(pos);
                    }
                }
            });
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.updateData(data);
        }
    }

    @Override
    public void addItemToList(ImageCard imageCard) {
        mAdapter.addItem(imageCard);
    }

    private void openDeleteDialog(final ImageCard imageCard) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Вы действительно хотите удалить фото?")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mAdapter.removeItem(imageCard);
                        imageCard.getFile().delete();
                        Snackbar.make(getView(), R.string.delete_photo_success, Snackbar.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }
}
