package com.example.andrey.myapplication.model;

import android.graphics.Bitmap;

import com.example.andrey.myapplication.helpers.Utils;

import java.io.File;
import java.io.Serializable;

/**
 * Модель одного элемента списка
 */
public class ImageCard implements Serializable{

    public static final int IMAGE_WIDTH = Utils.dpToPx(100);
    public static final int IMAGE_HEIGHT = Utils.dpToPx(100);

    private File mFile;
    private Bitmap mBitmap;
    private boolean mLike;

    public File getFile() {
        return mFile;
    }

    public void setFile(File file) {
        mFile = file;
    }

    public void setFileAndBitmap(File file) {
        mFile = file;
        mBitmap = Utils.getScaledBitmap(file.getAbsolutePath(), IMAGE_WIDTH, IMAGE_HEIGHT);
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public boolean isLike() {
        return mLike;
    }

    public void setLike(boolean like) {
        mLike = like;
    }
}
