package com.example.andrey.myapplication;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static App instance;
    private Context context;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        context = getApplicationContext();
    }

    public Context getContext() {
        return context;
    }
}
