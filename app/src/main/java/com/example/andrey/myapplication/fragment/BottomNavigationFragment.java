package com.example.andrey.myapplication.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.andrey.myapplication.R;
import com.example.andrey.myapplication.Router;
import com.example.andrey.myapplication.model.ImageCard;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BottomNavigationFragment extends Fragment
        implements BottomNavigationFragmentCallback {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView mBottomNavigation;

    private Unbinder mUnbinder;
    private Router mRouter;
    private ListPhotoTabFragment mListPhotoTabFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_bottom_navigation, container, false);
        mUnbinder = ButterKnife.bind(this, v);
        mBottomNavigation.setOnNavigationItemSelectedListener(getNavigationListener());
        mListPhotoTabFragment = ListPhotoTabFragment.newInstance(ListPhotoTabFragment.DISPLAY_TYPE_ALL);
        mRouter.replaceFragment(mListPhotoTabFragment, false);
        return v;
    }

    public static BottomNavigationFragment newInstance() {
        Bundle args = new Bundle();

        BottomNavigationFragment fragment = new BottomNavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRouter = new Router(R.id.bottom_nav_fragment_container, getFragmentManager());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener getNavigationListener() {
        return new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_common:
                        mListPhotoTabFragment = ListPhotoTabFragment.newInstance(ListPhotoTabFragment.DISPLAY_TYPE_ALL);
                        mRouter.replaceFragment(mListPhotoTabFragment, false);
                        return true;
                    case R.id.navigation_database:
                        return true;
                    case R.id.navigation_network:
                        return true;
                }
                return false;
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void updateCurrentFragment() {
        int selectedItem = mBottomNavigation.getSelectedItemId();
        switch (selectedItem) {
            case R.id.navigation_common:
                mListPhotoTabFragment.initRecycler();
                break;
            case R.id.navigation_database:
                // TODO
                break;
            case R.id.navigation_network:
                // TODO
                break;
            default:
                break;
        }
    }

    @Override
    public void addItemToList(ImageCard imageCard) {
        mListPhotoTabFragment.addItemToList(imageCard);
    }
}
