package com.example.andrey.myapplication.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.andrey.myapplication.R;
import com.example.andrey.myapplication.model.ImageCard;
import com.example.andrey.myapplication.model.ImageCardStorage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListPhotoFragment extends Fragment {

    public static final int REQUEST_PHOTO = 3;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.fab)
    FloatingActionButton mFloating;
    @BindView(R.id.coordinator)
    CoordinatorLayout mCoordinator;

    private Unbinder mUnbinder;
    private ListPhotoPagerAdapter mAdapter;
    private String mCurrentPhotoPath;
    private BottomNavigationFragment mBottomNavigationFragment;
    private ListPhotoTabFragment mTabFragment2;


    public static ListPhotoFragment newInstance() {
        Bundle args = new Bundle();

        ListPhotoFragment fragment = new ListPhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ListPhotoPagerAdapter(getFragmentManager());
        ImageCardStorage.getInstance().initData(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_photo, container, false);
        mUnbinder = ButterKnife.bind(this, v);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i) {
                    case 0:
                        mBottomNavigationFragment.updateCurrentFragment();
                        break;
                    case 1:
                        mTabFragment2.initRecycler();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        mTabLayout.setupWithViewPager(mViewPager);

        mFloating.setOnClickListener(view -> {
            mCurrentPhotoPath = null;
            dispatchTakePictureIntent();
        });
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_PHOTO) {
            if (mCurrentPhotoPath != null && mCurrentPhotoPath.length() > 0) {
                ImageCard imageCard = new ImageCard();
                imageCard.setFileAndBitmap(new File(mCurrentPhotoPath));
                ImageCardStorage.getInstance().addItem(imageCard);
                mBottomNavigationFragment.addItemToList(imageCard);
                Snackbar.make(mCoordinator, R.string.add_photo_success, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.example.andrey.myapplication.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public class ListPhotoPagerAdapter extends FragmentStatePagerAdapter {

        private List<String> mTabTitles;

        public ListPhotoPagerAdapter(FragmentManager fm) {
            super(fm);
            mTabTitles = new ArrayList<>();
            mTabTitles.add("Главная");
            mTabTitles.add("Избранное");
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTabTitles.get(position);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    mBottomNavigationFragment = BottomNavigationFragment.newInstance();
                    return mBottomNavigationFragment;
                case 1:
                    mTabFragment2 = ListPhotoTabFragment.newInstance(ListPhotoTabFragment.DISPLAY_TYPE_LIKE);
                    return mTabFragment2;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mTabTitles.size();
        }
    }
}
