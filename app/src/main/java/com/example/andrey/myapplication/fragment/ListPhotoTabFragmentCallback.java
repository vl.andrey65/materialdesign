package com.example.andrey.myapplication.fragment;

import com.example.andrey.myapplication.model.ImageCard;

interface ListPhotoTabFragmentCallback {
    void initRecycler();

    void addItemToList(ImageCard imageCard);
}
