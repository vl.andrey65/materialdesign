package com.example.andrey.myapplication.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Класс используется для управления сохраненными настройками приложения.
 */
public class PreferenceHelper {

    // ключи настроек
    public static final String PREF_THEME = "PREF_THEME";

    private static PreferenceHelper mInstance; // экземпляр данного класса
    private Context mContext;
    private SharedPreferences mSharedPreferences;

    // приватный конструктор
    private PreferenceHelper(Context context) {
        mContext = context;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public static PreferenceHelper getInstance(Context context) {
        if (mInstance == null)
            mInstance = new PreferenceHelper(context);
        return mInstance;
    }

    public int getTheme() {
        return mSharedPreferences.getInt(PREF_THEME, -1);
    }

    /**
     * Сохранение темы в настройках
     */
    public void saveTheme(int styleId) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putInt(PREF_THEME, styleId);
        edit.apply();
    }

    public boolean removeTheme() {
        return removePref(PREF_THEME);
    }

    public boolean removePref(String prefName) {
        if (!mSharedPreferences.contains(prefName))
            return false;
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.remove(prefName);
        edit.apply();
        return true;
    }
}