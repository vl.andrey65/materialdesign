package com.example.andrey.myapplication.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.andrey.myapplication.R;
import com.example.andrey.myapplication.activity.MainActivity;
import com.example.andrey.myapplication.helpers.PreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ThemeSelectionFragment extends Fragment {

    Unbinder mUnbinder;

    @BindView(R.id.blue_theme)
    Button mBlueTheme;
    @BindView(R.id.green_theme)
    Button mGreenTheme;
    @BindView(R.id.purple_theme)
    Button mPurpleTheme;

    public static ThemeSelectionFragment newInstance() {
        Bundle args = new Bundle();

        ThemeSelectionFragment fragment = new ThemeSelectionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_theme_selection, container, false);
        mUnbinder = ButterKnife.bind(this, v);

        initListeners();

        return v;
    }

    private void initListeners() {
        mBlueTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTheme(R.style.AppTheme_NoActionBar_Blue);
            }
        });

        mGreenTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTheme(R.style.AppTheme_NoActionBar_Green);
            }
        });

        mPurpleTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTheme(R.style.AppTheme_NoActionBar_Purple);
            }
        });
    }

    private void changeTheme(@StyleRes int styleId) {
        PreferenceHelper.getInstance(getActivity()).saveTheme(styleId);
        getActivity().finish();
        startActivity(MainActivity.newIntent(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}
