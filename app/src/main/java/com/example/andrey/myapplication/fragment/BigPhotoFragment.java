package com.example.andrey.myapplication.fragment;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;

import com.example.andrey.myapplication.helpers.Utils;
import com.example.andrey.myapplication.model.ImageCard;

import java.io.File;

public class BigPhotoFragment extends DialogFragment {

    private final static String ARG_PHOTO = "ARG_PHOTO";

    private ImageCard mPhoto;

    public static BigPhotoFragment newInstance(ImageCard photo) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_PHOTO, photo);

        BigPhotoFragment fragment = new BigPhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        mPhoto = (ImageCard) getArguments().getSerializable(ARG_PHOTO);

        ImageView imageView = new ImageView(getActivity());
        Bitmap bitmap = Utils.getScaledBitmap(mPhoto.getFile().getAbsolutePath(), getActivity());
        imageView.setImageBitmap(bitmap);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(imageView);

        return builder.create();
    }
}