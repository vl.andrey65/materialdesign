package com.example.andrey.myapplication;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class Router {

    private int mFragmentContainer;
    private FragmentManager mFragmentManager;

    public Router(int fragmentContainer, FragmentManager fragmentManager) {
        mFragmentContainer = fragmentContainer;
        mFragmentManager = fragmentManager;
    }

    public static void startActivity(Intent intent) {
        App.getInstance().startActivity(intent);
    }

    public void removeFragment() {
        mFragmentManager.popBackStack();
    }

    public void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(mFragmentContainer, fragment);
        if (addToBackStack) transaction.addToBackStack(null);
        transaction.commit();
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(mFragmentContainer, fragment);
        if (addToBackStack) transaction.addToBackStack(null);
        transaction.commit();
    }
}
